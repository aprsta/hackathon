package com.firstlinesoftware.hackathon.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.util.Base64.NO_WRAP
import android.view.MenuItem
import android.widget.Checkable
import androidx.appcompat.app.AppCompatActivity
import com.firstlinesoftware.hackathon.R
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_add_container.*
import java.io.ByteArrayOutputStream

class AddContainerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_container)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }

        registerListeners()
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode != CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) return

        if (resultCode == Activity.RESULT_OK) {
            val uri = CropImage.getActivityResult(data).uri ?: return
            addContainerAvatar.setImageURI(uri)
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            // Bad result.
        }
    }

    private fun registerListeners() {
        addContainerAvatar.setOnClickListener {
            CropImage.activity().apply {
                setGuidelines(CropImageView.Guidelines.OFF)
                setCropShape(CropImageView.CropShape.RECTANGLE)
                setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                setOutputCompressQuality(80)
                setAllowFlipping(false)
                start(this@AddContainerActivity)
            }
        }
    }

    private fun getCategories() = listOf<Checkable>(
        addContainerCategoryPaper, addContainerCategoryGlass,
        addContainerCategoryPlastic, addContainerCategoryMetal,
        addContainerCategoryClothes, addContainerCategoryOther,
        addContainerCategoryHazardous, addContainerCategoryBatteries,
        addContainerCategoryLamps, addContainerCategoryAppliances,
        addContainerCategoryTetra
    ).mapIndexedNotNull { index, button ->
        if (button.isChecked) index + 1 else null
    }

    companion object {
        fun bitmapFromUri(uri: String) = BitmapFactory.decodeFile(uri)

        fun base64FromBitmap(bitmap: Bitmap) = ByteArrayOutputStream().use { stream ->
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            Base64.encode(stream.toByteArray(), NO_WRAP)
        }

        fun bitmapFromBase64(base64: String) = Base64.decode(base64, NO_WRAP).let { decoded ->
            BitmapFactory.decodeByteArray(decoded, 0, decoded.size)
        }
    }
}

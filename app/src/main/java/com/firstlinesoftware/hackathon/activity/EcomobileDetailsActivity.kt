package com.firstlinesoftware.hackathon.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.firstlinesoftware.hackathon.R
import kotlinx.android.synthetic.main.activity_add_container.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class EcomobileDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ecomobile_details)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(false)
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    private fun formatSchedule(fromInSeconds: Long, toInSeconds: Long): String {
        val formatDate = SimpleDateFormat("d MMMMM", Locale.getDefault())
        val formatTime = SimpleDateFormat("hh:mm", Locale.getDefault())

        val millisecInSec = TimeUnit.SECONDS.toMillis(1)
        val fromDate = Date(fromInSeconds * millisecInSec)
        val toDate = Date(toInSeconds * millisecInSec)

        val date = formatDate.format(fromDate)
        val timeFrom = formatTime.format(fromDate)
        val timeTo = formatTime.format(toDate)

        return getString(R.string.ecomobile_details_schedule_time_format, date, timeFrom, timeTo)
    }
}
